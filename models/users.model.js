const mongoose = require('mongoose');
const schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const userModel = schema({
    email: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ["ADMIN", "USER"],
        default: "USER"
    }
}, {
    timestamps: true,
})

userModel.pre('save', function (next) {

    this.password = bcrypt.hashSync(this.password, 10);
    next();
})

userModel.methods.show = function () {
    let { password, _id, ...user } = this._doc;

    return user;
}


module.exports = mongoose.model('user', userModel)