const jwt = require('jsonwebtoken');

module.exports = (id, role) => {
    const token = jwt.sign({
        _id: id,
        role: role
    }, process.env.JWT_SECRET);
    return token;
}