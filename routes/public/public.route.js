const router = require('express').Router();

router.get('/', function(req,res,next){
    res.status(200).json('You can Access this route only after signed in')
})

module.exports = router;