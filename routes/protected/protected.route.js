const router = require('express').Router();

router.get('/', function (req, res, next) {
    res.status(200).json(`Your role is ${req.user.role}`)
})


module.exports = router;