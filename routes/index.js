const router = require('express').Router();
const isAdminMiddleware = require('../middlewares/isAdmin.middleware');
const isUserMiddleware = require('../middlewares/isUser.middleware');
const authRoute = require('./auth/auth.route');
const protectedRoute = require('./protected/protected.route');
const publicRoute = require('./public/public.route')
router.use('/auth', authRoute);
router.use('/protected',isUserMiddleware, isAdminMiddleware, protectedRoute )
router.use('/public', isUserMiddleware, publicRoute)



module.exports = router;