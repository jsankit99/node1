const UserModel = require('../../models/users.model');
const bcrypt = require('bcrypt');
const jwt_sign = require('../../common/jwt_sign');

const router = require('express').Router();


router.post('/signup', function (req, res, next) {
    let { email, password, role } = req.body;
    let newUser = new UserModel({
        email, password, role
    });
    newUser.save().then(data => {
        res.status(200).json('Successfully Signed Up')
    })
        .catch(err => next(err))
})

router.post('/signin', function (req, res, next) {
    let { email, password } = req.body;
    UserModel.findOne({ email })
        .exec(function (err, user) {
            if (err) return next(err)
            if (!user) return next({ msg: 'User not Found!' })
            const correct = bcrypt.compareSync(password, user.password);
            if (correct) {
                const token = jwt_sign(user._id, user.role);
           
                res.status(200).json({
                    token, user: user.show()
                })
            }
            else {
                return next({
                    msg: 'Password Incorrect'
                })
            }
        })
})




module.exports = router;