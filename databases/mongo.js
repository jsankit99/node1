const mongoose = require('mongoose');


mongoose.connect(process.env.MONGO_URI, function(err){
   
    if(err){
        console.log('Error Connecting to DB')
    }
    else{
        console.log('Connected to MONGO DB')
    }
})