const UserModel = require("../models/users.model");
const jwt = require('jsonwebtoken')
module.exports = async (req, res, next) => {
    const token = req.headers['authorization'];

    if (token) {
        try {
            const decrypted = await jwt.verify(token, process.env.JWT_SECRET)
            UserModel.findById(decrypted._id).then(user => {

                if (!user) {
                    return next({
                        msg: 'No User Found!'
                    })
                }
                req.user = user.show();
                next();
            })
                .catch(err => {
                    return next(err)
                })

        }
        catch (e) {
            return next(e)
        }
    }
    else {
        return next({
            msg: 'Not Signed In!'
        })
    }
}