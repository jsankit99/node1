module.exports = (req, res, next) => {
    if (req.user.role === 'ADMIN') {
        next();
    }
    else {
        return next({
            msg: 'Not Authorized!'
        })
    }
}